# README

An attempt to recreate the Halloween countdown clock from the movie Nightmare Before Christmas.

# Project Outline

 - Hardware Discovery
 	- Controller
 	- Wheel Unit
 		- Motor
 			- Motor control
 		- Wheel
 		- housing
 		- mounting
	- Power
 - Software
 	- Motor/Wheel Positioning
 	- Determining How many days left
 	- Testing Reliability
 	- Controller interfacing with Paripherals

# Hardware

## Raspberry pi

An rPi was chosen since it is cheap, networked (ntp), and has good support for gpio. Due to the nature of the clock (stationary), having a constant power supply is not an issue. 

## Number Wheels

Number wheels are required as displays. They contain the number 0 to 9. However, only the 1's place will display every number (as there are only 365 days in the year).

I am thinking of making each wheel an independant and addressable unit. I2C seems to make the most sense. In this way, I could extend how many wheels that I have to the max supported by I2C. 

### Construction

I'm thinking that I should be able to cast the wheels out of resin. I'd really like to 3d print them, however I do not have a 3d printer. This project is not worth the $2k for a printer. I could cnc a mold cavity for them, however I do not have a cnc. 

### Bill of Materials per unit

 - ATTiny85
 - Stepper Motor Driver
 - Wheel
 - Housing

# Software

## UpdateCounter.py

This is the main script. It performs the following:

 - Calculates the days before Halloween (localtime)
 - Gets the current state of the wheels
 - Updates each wheel indipendantly (begins with 1's pace)

## Chron

UpdateCounter.py is run every night at 12:01 via chron. 

## ATTiny85

 - I2C slave
 - Controls motors (drives them)
 - reads encoding

I2C Slave discovery is performed by brute force.

### API

 - int getDialPlace(void)
 	- returns 0-9
 - int setDialPlace(int place)
 	- accepts 0-9
 	- returns error code