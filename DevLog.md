# DevLog

## April 5th

I've decided to keep a dev log for this project. I don't think that I've ever kept a dev log before. Essentially I'll just write down things that I think of over the course of this project. Nothing terribly exciting. Maybe it'll prove interesting; Maybe not. 

I made a very rough prototype wheel unit out of cardboard. I think the Drum style wheel will work. The housing needs a more complete design. The problem will be constructing the housing. I need to be able to cut metal on right angles effectively. A Shear Break Roll machine would be great for that. I really only need a Shear and Break.  

I still think that I should be able to cast the drum wheels out of resin. However, I'm not sure how I'll be able to create the initial form. If I had a 3d printer, then this wouldn't be an issue. However, I cannot afford a 3d printer right now. Maybe I should look into getting one 3d printed from shapeways. If I had a lathe, I would be able to create the form. However, the cost of a lathe + stock might be comparable to buying a 3d printer. 

The drum wheel is a 4"dia wheel that has a 1.25" protrusion on the edge and center (hollowed out). I was thinking of just gluing a flywheel on the outside center to connect the motor to. Alternatively, we could attach the wheel to the shaft, then attach a flywheel to the shaft. 

Alternatively, making some circles out of plywood would also do the trick. In fact, 2 layers of .75" ply would be the right thickness. However, I still run into the issue of how do I make them perfectly circular. I could try turning the drill press into a lathe, but then I'd ruin the center.

## May 4th

No more progress has been made. There are many roadblocks that I'm unsure how to solve.
I want a 3d printer. This shit would be easy with one. 

## June 22nd

No progress, see May 4th.