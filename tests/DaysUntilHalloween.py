#!/usr/bin/env python
import datetime


def daysUntilHalloween(fromTime):
    currYear = datetime.date.today().year
    halloween = datetime.date(currYear, 10, 31)
    delta = halloween - fromTime

    return delta.days


def main():
    try:
        # if current year is greater than YEAR
        # increase year by 1
        print daysUntilHalloween(datetime.date.today())
        pass
    except(KeyboardInterrupt):
        print 'Stopping...'

    except Exception as e:
        print type(e)
        print 'ERROR - ', e

if __name__ == '__main__':
    main()
